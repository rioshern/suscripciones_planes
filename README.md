

Para ejecutar las pruebas en ambiente local, primero necesitamos tener una instancia de MariaDB y RabbitMQ.

```bash
cd pruebas
docker-compose up -d
```

Luego ejecutar la aplicación de Spring Boot

```bash
cd ..
mvn spring-boot:run -pl web
```

Invocar el servicio

```curl
curl -X PUT 'http://localhost:8080/localidades/2332' \
-H 'Content-Type: application/json' \
--data-raw '{
    "nombre": "Tandil",
    "codigoPostal": 7000
}'
```

- [Guías de desarrollo](docs/README.md)
