package com.ma.shared;

import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KeycloakResolverConfig {

    /**
     * Ver https://github.com/keycloak/keycloak/issues/8857
     * @return org.keycloak.adapters.KeycloakConfigResolver
     */
    @Bean
    public KeycloakConfigResolver myKeycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }
}
