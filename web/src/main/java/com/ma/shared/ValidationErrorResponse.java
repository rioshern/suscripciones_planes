package com.ma.shared;

import java.util.ArrayList;
import java.util.List;

public class ValidationErrorResponse {

    private List<ValidationError> errors = new ArrayList<>();

    public List<ValidationError> getErrors() {
        return errors;
    }

    public void setErrors(List<ValidationError> errors) {
        this.errors = errors;
    }
}