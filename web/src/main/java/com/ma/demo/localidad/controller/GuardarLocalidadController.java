package com.ma.demo.localidad.controller;

import com.ma.demo.localidad.application.GuardarLocalidad;
import com.ma.demo.localidad.domain.LocalidadCodigoPostal;
import com.ma.demo.localidad.domain.LocalidadInder;
import com.ma.demo.localidad.domain.LocalidadNombre;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@RestController
@Validated
public class GuardarLocalidadController {

    Logger logger = LoggerFactory.getLogger(GuardarLocalidadController.class);

    private final GuardarLocalidad guardarLocalidad;

    public GuardarLocalidadController(final GuardarLocalidad guardarLocalidad) {
        this.guardarLocalidad = guardarLocalidad;
    }

    @PutMapping("/localidades/{inder}")
    public void guardar(@PathVariable @Min(1) @Max(99999) Integer inder,
                        @Valid @RequestBody GuardarLocalidadRequest request) {
        logger.debug("guardando localidad {} con los datos {}" , inder, request);
        guardarLocalidad.guardar(new LocalidadInder(inder), new LocalidadNombre(request.getNombre()),
                new LocalidadCodigoPostal(request.getCodigoPostal()));
    }
}
