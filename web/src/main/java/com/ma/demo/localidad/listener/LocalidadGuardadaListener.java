package com.ma.demo.localidad.listener;

import com.ma.demo.localidad.domain.LocalidadGuardada;
import com.ma.shared.domain.IntegrationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class LocalidadGuardadaListener {

    Logger logger = LoggerFactory.getLogger(LocalidadGuardadaListener.class);

    @EventListener
    public void onLocalidadGuardada(LocalidadGuardada event) {
        logger.info("Evento de localidad {}", event.eventName());
    }

    @EventListener
    public void onIntegrationEvent(IntegrationEvent event) {
        logger.info("Integration Event {}", event.getExchangeName());
    }
}
