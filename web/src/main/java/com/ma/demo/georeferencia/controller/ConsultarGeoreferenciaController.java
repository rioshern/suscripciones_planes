package com.ma.demo.georeferencia.controller;

import com.ma.demo.georeferencia.application.ConsultarGeoreferencia;
import com.ma.shared.domain.EntityNotFound;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsultarGeoreferenciaController {

    private final ConsultarGeoreferencia consultarGeoreferencia;

    public ConsultarGeoreferenciaController(final ConsultarGeoreferencia consultarGeoreferencia) {
        this.consultarGeoreferencia = consultarGeoreferencia;
    }

    @GetMapping("/geo")
    public GeoreferenciaReponse consultar(@RequestParam(name = "nombre", required = true) String nombre) {
        return consultarGeoreferencia
                    .consultar(nombre)
                    .map(GeoreferenciaReponse::fromDomain)
                .orElseThrow(() -> new EntityNotFound(String.format("Georeferencia no encontrada con el nombre %s", nombre)));
    }
}
