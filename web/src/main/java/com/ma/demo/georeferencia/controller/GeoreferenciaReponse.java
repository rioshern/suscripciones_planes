package com.ma.demo.georeferencia.controller;

import com.ma.demo.georeferencia.domain.Georeferencia;
import com.ma.demo.georeferencia.domain.GeoreferenciaPunto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeoreferenciaReponse {
    private String nombre;
    private Double longitud;
    private Double latitud;

    public static GeoreferenciaReponse fromDomain(Georeferencia georeferencia) {
        GeoreferenciaPunto punto = georeferencia.getPunto();
        return new GeoreferenciaReponse(georeferencia.getNombre().getValor(),
                                        punto.getLongitud(), punto.getLatitud());

    }
}
