package com.ma;

import com.ma.scripts.pom.service.ChangeArtifact;
import com.ma.scripts.pom.service.PomFileRepository;
import com.ma.scripts.pom.service.Replacements;
import com.ma.scripts.shared.ConsoleUtils;

import java.util.List;

public class App {

    private static final String CURRENT_PATH = System.getProperty("user.dir");

    public static void main(String[] args) {
        String proyectName = getProjectName(args);
        if (proyectName == null) {
            ConsoleUtils.println("Must set project name");
            System.exit(1);
        } else {
            ConsoleUtils.println("changing project name: " + getProjectName(args));
            PomFileRepository repository = new PomFileRepository();
            Replacements replacements = new Replacements(proyectName);
            ChangeArtifact changeArtifact = new ChangeArtifact(repository, replacements);
            List<String> pomPaths = repository.getPaths(CURRENT_PATH);
            pomPaths.forEach(changeArtifact::changePom);
        }
    }

    private static String getProjectName(String[] args) {
        return args.length == 0 ? null : args[0];
    }
}