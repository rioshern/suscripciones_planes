package com.ma.scripts.shared;

import java.io.Console;

public class ConsoleUtils {

    private ConsoleUtils() {}

    public static void println(String message) {
        System.console().printf(message + "%n");
    }


}
