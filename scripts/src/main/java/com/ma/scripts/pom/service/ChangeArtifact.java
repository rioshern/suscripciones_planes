package com.ma.scripts.pom.service;

import com.ma.scripts.shared.ConsoleUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ChangeArtifact {

    private final PomFileRepository pomRepository;
    private final Replacements replacements;

    public ChangeArtifact(PomFileRepository pomRepository, Replacements replacements) {
        this.pomRepository = pomRepository;
        this.replacements = replacements;
    }    

    public void copyPom() {
        List<String> lines = pomRepository.getLines("pom2.xml");
        List<String> linesReplaced = lines.stream().map(this::searchReplace).collect(Collectors.toList());
        pomRepository.saveLines("pom2.xml", linesReplaced);
    }

    public void changePom(String path) {
        ConsoleUtils.println("Change " + path);
        List<String> lines = pomRepository.getLines(path);
        List<String> linesReplaced = lines.stream().map(this::searchReplace).collect(Collectors.toList());
        pomRepository.saveLines(path, linesReplaced);
    }

    private String searchReplace(String line) {
        for (Map.Entry<String, String> entry : replacements.getReplacements().entrySet()) {
            if (line.contains(entry.getKey())) {
                line = line.replace(entry.getKey(), entry.getValue());
            }
        }
        
        return line;
    }
    
}
