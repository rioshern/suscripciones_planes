package com.ma.shared.domain;

public class EntityNotFound extends RuntimeException {
    public EntityNotFound(String msg) {
        super(msg);
    }
}
