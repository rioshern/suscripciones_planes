package com.ma.shared.domain;

import java.io.Serializable;
import java.util.Map;

public interface IntegrationEvent {
    String getExchangeName();

    String eventName();
    String aggregateId();

    String eventId();

    String occurredOn();

    Map<String, Serializable> data();

}
