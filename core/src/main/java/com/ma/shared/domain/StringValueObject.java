package com.ma.shared.domain;

import java.util.Objects;

public class StringValueObject {

    private final String valor;

    public StringValueObject(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StringValueObject)) return false;
        StringValueObject that = (StringValueObject) o;
        return Objects.equals(valor, that.valor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(valor);
    }
}
