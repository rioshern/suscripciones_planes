package com.ma.demo.georeferencia.domain;

import java.util.Optional;

public interface GeoreferenciaRepository {
    Optional<Georeferencia> buscarPorNombre(String nombre);
}
