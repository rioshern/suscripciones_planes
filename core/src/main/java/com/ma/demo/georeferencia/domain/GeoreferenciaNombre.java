package com.ma.demo.georeferencia.domain;

import com.ma.shared.domain.StringValueObject;

public class GeoreferenciaNombre extends StringValueObject {
    public GeoreferenciaNombre(String valor) {
        super(valor);
        validarNombre(valor);
    }

    private void validarNombre(String valor) {
        if (valor == null || valor.length() == 0) {
            throw new IllegalArgumentException("Nombre no válido");
        }
    }

    @Override
    public String toString() {
        return "GeoreferenciaNombre{valor=" + getValor() + "}";
    }

}
