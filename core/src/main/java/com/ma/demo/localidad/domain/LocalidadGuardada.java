package com.ma.demo.localidad.domain;

import com.ma.shared.domain.DomainEvent;
import com.ma.shared.domain.IntegrationEvent;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LocalidadGuardada extends DomainEvent implements IntegrationEvent {

    public static final String EVENT_NAME = "dummy.enviado";
    public static final String EXCHANGE_NAME = "dummy";

    private final Integer inder;
    private final String nombre;
    private final Integer codigoPostal;

    public LocalidadGuardada(Integer inder, String nombre, Integer codigoPostal) {
        super(inder.toString());
        this.inder = inder;
        this.nombre = nombre;
        this.codigoPostal = codigoPostal;
    }

    @Override
    public String eventName() {
        return EVENT_NAME;
    }

    @Override
    public Map<String, Serializable> data() {
        Map<String, Serializable> eventData = new HashMap<>();
        eventData.put("inder", inder);
        eventData.put("nombre", nombre);
        eventData.put("codigo_postal", codigoPostal);
        return  eventData;
    }

    @Override
    public String getExchangeName() {
        return EXCHANGE_NAME;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LocalidadGuardada)) return false;
        LocalidadGuardada that = (LocalidadGuardada) o;
        return Objects.equals(inder, that.inder) && Objects.equals(nombre, that.nombre) &&
               Objects.equals(codigoPostal, that.codigoPostal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), inder, nombre, codigoPostal);
    }

}
