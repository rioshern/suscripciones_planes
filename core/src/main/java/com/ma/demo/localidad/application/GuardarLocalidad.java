package com.ma.demo.localidad.application;

import com.ma.demo.localidad.domain.*;
import com.ma.shared.domain.EventBus;
import com.ma.shared.domain.Service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class GuardarLocalidad {

    Logger logger = LoggerFactory.getLogger(GuardarLocalidad.class);

    private final LocalidadRepository localidadRepository;
    private final EventBus eventBus;

    public GuardarLocalidad(final LocalidadRepository localidadRepository, final EventBus eventBus) {
        this.localidadRepository = localidadRepository;
        this.eventBus = eventBus;
    }

    public void guardar(LocalidadInder inder, LocalidadNombre nombre, LocalidadCodigoPostal codigoPostal) {
        Localidad localidad = Localidad.guardar(inder, nombre, codigoPostal);
        localidadRepository.guardar(localidad);
        eventBus.publish(localidad.pullEvents());
        logger.info("Localidad guardada: {}", localidad.getInder().getValor());
    }
}
