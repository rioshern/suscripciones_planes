package com.ma.demo.localidad.domain;

public interface LocalidadRepository {
    void guardar(Localidad localidad);
}
