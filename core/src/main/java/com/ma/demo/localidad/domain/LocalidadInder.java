package com.ma.demo.localidad.domain;

import com.ma.shared.domain.IntValueObject;

public class LocalidadInder extends IntValueObject {

    public LocalidadInder(Integer valor) {
        super(valor);
        validarInder(valor);
    }

    private void validarInder(Integer valor)  {
        if (valor == null || valor < 0) {
            throw new IllegalArgumentException("Inder no válido");
        }
    }

    @Override
    public String toString() {
        return "LocalidadInder{valor=" + getValor() + "}";
    }
}
