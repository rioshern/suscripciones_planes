package com.ma.demo.georeferencia.application;


import com.ma.demo.georeferencia.domain.Georeferencia;
import com.ma.demo.georeferencia.domain.GeoreferenciaNombre;
import com.ma.demo.georeferencia.domain.GeoreferenciaPunto;

public class GeoreferenciaMother {

    public static Georeferencia getEjemplo() {
        GeoreferenciaPunto punto = new GeoreferenciaPunto(-37.3238849060878, -59.1310691770429);
        GeoreferenciaNombre nombre = new GeoreferenciaNombre("TANDIL");
        return new Georeferencia(punto, nombre);
    }
}
