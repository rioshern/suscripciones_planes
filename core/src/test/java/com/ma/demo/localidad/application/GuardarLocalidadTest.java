package com.ma.demo.localidad.application;

import com.ma.demo.localidad.domain.Localidad;
import com.ma.demo.localidad.domain.LocalidadRepository;
import com.ma.shared.domain.DomainEvent;
import com.ma.shared.domain.EventBus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.mockito.Mockito.*;

class GuardarLocalidadTest {

    private LocalidadRepository localidadRepository;
    private EventBus eventBus;
    private GuardarLocalidad guardarLocalidad;

    @BeforeEach
    void setUp() {
        localidadRepository = mock(LocalidadRepository.class);
        eventBus = mock(EventBus.class);
        guardarLocalidad = new GuardarLocalidad(localidadRepository, eventBus);
    }

    @Test
    void deberia_guardar_una_localidad_valida() {
        Localidad localidad = LocalidadMother.ejemplo();
        guardarLocalidad.guardar(localidad.getInder(), localidad.getNombre(), localidad.getCodigoPostal());
        deberiaGuardarse(localidad);
        deberiaPublicarse(LocalidadGuardadaMother.ejemplo(localidad));
    }

    void deberiaGuardarse(Localidad localidad) {
        verify(localidadRepository, atLeastOnce()).guardar(localidad);
    }

    void deberiaPublicarse(List<DomainEvent> events) {
        verify(eventBus, atLeastOnce()).publish(events);
    }

}