package com.ma.demo.localidad.application;

import com.ma.demo.localidad.domain.Localidad;
import com.ma.demo.localidad.domain.LocalidadCodigoPostal;
import com.ma.demo.localidad.domain.LocalidadInder;
import com.ma.demo.localidad.domain.LocalidadNombre;

public class LocalidadMother {

    public static Localidad ejemplo() {
        return  new Localidad(new LocalidadInder(1234), new LocalidadNombre("SANTA FE"),
                new LocalidadCodigoPostal(3000));
    }
}
