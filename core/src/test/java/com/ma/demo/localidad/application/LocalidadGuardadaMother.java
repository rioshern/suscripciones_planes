package com.ma.demo.localidad.application;

import com.ma.demo.localidad.domain.Localidad;
import com.ma.demo.localidad.domain.LocalidadGuardada;
import com.ma.shared.domain.DomainEvent;

import java.util.Collections;
import java.util.List;

public class LocalidadGuardadaMother {

    public static List<DomainEvent> ejemplo(Localidad localidad) {
        LocalidadGuardada event = new LocalidadGuardada(localidad.getInder().getValor(),
                                                        localidad.getNombre().getValor(),
                                                        localidad.getCodigoPostal().getValor());
        return Collections.singletonList(event);
    }
}
