# Agregados
Un agregado es la clase principal de un dominio, puede ser en si misma una entidad o agrupar varias entidades y value objects.

Un agregado impone un límite de consistencia.

Un agregado encapsula la lógica de negocio.

## Implementación
Una clase de agregado debe crearse en el paquete *domain*. El nombre del agregado debe ser igual a la entidad de negocio, por ejemplo: Factura, Cuota, Poliza, Cliente, etc. 

Un agregado puede extender la clase ```com.ma.shared.domain.AggregateRoot```

## Registrar eventos.
Cada vez que necesitar registrar un evento de dominio podemos invocar al método *push* si nuestra clase hereda comportamiento de *AggregateRoot*.

Este método registra un evento de tipo *DomainEvent* en la lista de eventos para ser publicada postermente en un bus. 

Ejemplo:

```java
    public static Localidad guardar(LocalidadInder inder, 
                                    LocalidadNombre nombre, 
                                    LocalidadCodigoPostal codigoPostal) {
        Localidad localidad = new Localidad(inder, nombre, codigoPostal);
        localidad.push(new LocalidadGuardada(inder.getValor(), 
                                             nombre.getValor(), 
                                             codigoPostal.getValor()));
        return  localidad;
    }
```

