# Repositorio

Se utiliza el patrón repositorio para persistir y consultar agregados.

Se debe crear un repositorio por agregado. Un repositorio puede tener todas las operaciones de busqueda, consulta o persistencia. 

Los repositorios se escriben como interfaces o clases abstractas y se almacenan en el paquete *domain* en el módulo *core* junto al agregado. 

Los repositorios se deben implementar en el módulo *infra* y pueden ser de diferente tipos: de bases de datos, cliente de servicios externos, almacenamiento de archivos, etc. 

El nombre del repositorio tendrá la estructura:

```
<Nombre de agregado>Repository
```

Ejemplo:

```java
public interface GeoreferenciaRepository {
    Optional<Georeferencia> buscarPorNombre(String nombre);
}
```





