# Jerarquía de paquetes
El proyecto tendrá la siguiente jerarquía de paquetes: 

```
com.ma.<dominio>.<agregado>.<capa>
```

## Dominios

El primer nivel de separación corresponde a los diferentes dominios. Por ejemplo: póliza, cobranza, impuestos, etc. En general un microservicio no debería tener más de un dominio. 

Existe el paquete *shared*, que corresponde con todas clases que pueden ser compartidas por más de un dominio.

## Agregados

El segundo nivel de separación está relacionado a un agregado o entidad principal. Por ejemplo para el caso del dominio de cobranza tenemos los agregados: cuota, deuda, cupón, canal, etc.

## Capas

El último nivel de separación es por capas, es decir por rol o responsabilidad de las clases.

Para el módulo core tenemos las siguientes capas: 

- *domain*: Se incluyen todas las clases de dominio: agregados, entidades, eventos, etc.
- *application*: Se incluyen todas las clases de servicio de aplicación.

Para el módulo infra tenemos las siguientes capas: *dao* para persistencia y *client* para implementar clientes de servicios. 

Para el módulo web: *controller* para implementar los controladores y dtos, *listener* para implementar suscriptores a eventos de dominio.

Evitar hacer paquetes muy específicos por ejemplo un paquete para los dtos, otro para las excepciones. Sino que debemos agrupar las clases por capas, aunque tengan distinto comportamiento. 

Ver [Proyecto maven multi módulo](multi-modulo.md)




