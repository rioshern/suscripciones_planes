

# Guías de desarrollo

- [Proyecto maven multi módulo](guias/multi-modulo.md)
- [Jerarquía de paquetes](guias/paquetes.md)
- [Servicios de aplicación](guias/servicios-aplicacion.md)
- [Repositorios](guias/repositorios.md)
- [Agregados](guias/agregados.md)