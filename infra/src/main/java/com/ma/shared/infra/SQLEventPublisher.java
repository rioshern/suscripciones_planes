package com.ma.shared.infra;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ma.shared.domain.IntegrationEvent;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Map;

@Component
public class SQLEventPublisher {

    private static final String SQL =
            "insert into domain_events (id, aggregate_id, name, body) " +
            "values (:id, :aggregateId, :name, :body);";

    private final EntityManager entityManager;

    public SQLEventPublisher(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public void publish(IntegrationEvent event) {
        String id = event.eventId();
        String aggregateId = event.aggregateId();
        String name = event.eventName();
        Map<String, Serializable> data = event.data();

        Query query = entityManager.createNativeQuery(SQL);
        query.setParameter("id", id);
        query.setParameter("aggregateId", aggregateId);
        query.setParameter("name", name);
        query.setParameter("body", jsonEncode(data));

        query.executeUpdate();
    }

    private String jsonEncode(Map<String, Serializable> data) {
        try {
            return new ObjectMapper().writeValueAsString(data);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

}
