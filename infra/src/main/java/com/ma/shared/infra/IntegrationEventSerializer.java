package com.ma.shared.infra;

import com.ma.shared.domain.IntegrationEvent;

import java.util.HashMap;
import java.util.Map;

public class IntegrationEventSerializer {

    public static Map<String, Object> serialize(IntegrationEvent integrationEvent) {
        Map<String, Object> event = new HashMap<>();
        event.put("id", integrationEvent.eventId());
        event.put("name", integrationEvent.eventName());
        event.put("occurred_on", integrationEvent.occurredOn());
        event.put("data", integrationEvent.data());
        return event;
    }
}
