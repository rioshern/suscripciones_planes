package com.ma.shared.config.rabbitmq;

import com.ma.shared.infra.YamlPropertySourceFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "rabbitmq")
@PropertySource(value = "classpath:rabbitmq.yaml", factory = YamlPropertySourceFactory.class)
public class RabbitProperties {

    private BindingProperty deadLetter;
    private QueueProperty defaultQueue;
    private List<String> exchanges;
    private List<QueueProperty> queues;
    private List<BindingProperty> bindings;

    public BindingProperty getDeadLetter() {
        return deadLetter;
    }

    public void setDeadLetter(BindingProperty deadLetter) {
        this.deadLetter = deadLetter;
    }

    public QueueProperty getDefaultQueue() {
        return defaultQueue;
    }

    public void setDefaultQueue(QueueProperty defaultQueue) {
        this.defaultQueue = defaultQueue;
    }

    public List<String> getExchanges() {
        return exchanges;
    }

    public void setExchanges(List<String> exchanges) {
        this.exchanges = exchanges;
    }

    public List<QueueProperty> getQueues() {
        return queues;
    }

    public void setQueues(List<QueueProperty> queues) {
        this.queues = queues;
    }

    public List<BindingProperty> getBindings() {
        return bindings;
    }

    public void setBindings(List<BindingProperty> bindings) {
        this.bindings = bindings;
    }

    @Override
    public String toString() {
        return "RabbitProperties{" +
                "deadLetter=" + deadLetter +
                ", defaultQueue=" + defaultQueue +
                ", exchanges=" + exchanges +
                ", queues=" + queues +
                ", bindings=" + bindings +
                '}';
    }
}
