package com.ma.shared.config.rabbitmq;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQExchanges {

    private final RabbitProperties rabbitProperties;

    public RabbitMQExchanges(RabbitProperties rabbitProperties) {
        this.rabbitProperties = rabbitProperties;
    }

    public List<Exchange> exchanges() {
        List<String> exchanges =  rabbitProperties.getExchanges();
        if (exchanges == null) {
            return Collections.emptyList();
        }
        return exchanges.stream().map(this::exchange).collect(Collectors.toList());
    }

    private Exchange exchange(String exchangeName) {
        return new TopicExchange(exchangeName);
    }
    
}
