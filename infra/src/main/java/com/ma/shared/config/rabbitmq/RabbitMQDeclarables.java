package com.ma.shared.config.rabbitmq;

import org.springframework.amqp.core.Declarable;
import org.springframework.amqp.core.Declarables;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;

@Configuration
public class RabbitMQDeclarables {

    private final RabbitMQDeadLetter deadLetter;
    private final RabbitMQExchanges exchanges;
    private final RabbitMQQueues queues;
    private final RabbitMQBindings bindings;

    public RabbitMQDeclarables(RabbitMQDeadLetter deadLetter, RabbitMQExchanges exchanges,
                               RabbitMQQueues queues, RabbitMQBindings bindings) {
        this.deadLetter = deadLetter;
        this.exchanges = exchanges;
        this.queues = queues;
        this.bindings = bindings;
    }

    @Bean
    public Declarables declarables() {
        Collection<Declarable> list = new ArrayList<>();
        list.addAll(deadLetter.declarables());
        list.addAll(exchanges.exchanges());
        list.addAll(queues.queues());
        list.addAll(bindings.bindings());
        return new Declarables(list);
    }
}
