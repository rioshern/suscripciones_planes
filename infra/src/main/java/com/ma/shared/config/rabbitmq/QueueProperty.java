package com.ma.shared.config.rabbitmq;

public class QueueProperty {
    private String name;
    private Boolean durable;
    private Boolean exclusive;
    private Boolean autoDelete;
    private Boolean deadLetter;

    public String getName() {
        return name;
    }

    public String getName(String defaultValue) {
        return name != null? name : defaultValue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDurable() {
        return durable;
    }

    public Boolean getDurable(Boolean defaultValue) {
        return durable != null? durable : defaultValue;
    }

    public void setDurable(Boolean durable) {
        this.durable = durable;
    }

    public Boolean getExclusive() {
        return exclusive;
    }

    public Boolean getExclusive(Boolean defaultValue) {
        return exclusive != null ? exclusive : defaultValue;
    }

    public void setExclusive(Boolean exclusive) {
        this.exclusive = exclusive;
    }

    public Boolean getAutoDelete() {
        return autoDelete;
    }

    public Boolean getAutoDelete(Boolean defaultValue) {
        return autoDelete != null? autoDelete: defaultValue;
    }

    public void setAutoDelete(Boolean autoDelete) {
        this.autoDelete = autoDelete;
    }

    public Boolean getDeadLetter() {
        return deadLetter;
    }

    public Boolean getDeadLetter(Boolean defaultValue) {
        return deadLetter != null? deadLetter : defaultValue ;
    }

    public void setDeadLetter(Boolean deadLetter) {
        this.deadLetter = deadLetter;
    }

    public QueueProperty getPropertiesOrDefaultValue(QueueProperty defaultProperty) {
        QueueProperty property = new QueueProperty();
        property.setName(this.getName(defaultProperty.getName()));
        property.setDurable(this.getDurable(defaultProperty.getDurable()));
        property.setExclusive(this.getExclusive(defaultProperty.getExclusive()));
        property.setAutoDelete(this.getAutoDelete(defaultProperty.getAutoDelete()));
        property.setDeadLetter(this.getDeadLetter(defaultProperty.getDeadLetter()));
        return property;
    }

    @Override
    public String toString() {
        return "QueueProperty{" +
                "name='" + name + '\'' +
                ", durable=" + durable +
                ", exclusive=" + exclusive +
                ", autoDelete=" + autoDelete +
                ", deadLetter=" + deadLetter +
                '}';
    }
}
