package com.ma.demo.georeferencia.client;

import lombok.Data;

@Data
public class CentroideResponse {
    private Double lat;
    private Double lon;
}
