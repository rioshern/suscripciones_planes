package com.ma.demo.georeferencia.client;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class GeoreferenciaResponse {
    private Integer cantidad;
    private Integer inicio;
    private List<LocalidadResponse> localidades;
    private Map<String, String> parametros;
    private Integer total;
}
