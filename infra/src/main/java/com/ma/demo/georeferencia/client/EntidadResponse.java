package com.ma.demo.georeferencia.client;

import lombok.Data;

@Data
public class EntidadResponse {
    private String id;
    private String nombre;
}
