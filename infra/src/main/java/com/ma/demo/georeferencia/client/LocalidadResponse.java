package com.ma.demo.georeferencia.client;

import lombok.Data;

@Data
public class LocalidadResponse {
    private String id;
    private String nombre;
    private String categoria;
    private CentroideResponse centroide;
    private EntidadResponse departamento;
    private EntidadResponse localidadCensal;
    private EntidadResponse municipio;
    private EntidadResponse provincia;
}
