package com.ma.demo.georeferencia.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "georeferencia", url = "https://apis.datos.gob.ar/georef/api/")
public interface GeoreferenciaClient {

    @GetMapping("/localidades")
    GeoreferenciaResponse getGeoreferencia(@RequestParam(name = "nombre") String nombre);
}
