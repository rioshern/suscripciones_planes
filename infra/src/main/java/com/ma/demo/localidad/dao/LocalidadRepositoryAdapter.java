package com.ma.demo.localidad.dao;

import com.ma.demo.localidad.domain.Localidad;
import com.ma.demo.localidad.domain.LocalidadRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public class LocalidadRepositoryAdapter implements LocalidadRepository {

    Logger logger = LoggerFactory.getLogger(LocalidadRepositoryAdapter.class);

    private final LocalidadRepositoryJPA repositoryJPA;

    public LocalidadRepositoryAdapter(LocalidadRepositoryJPA repositoryJPA) {
        this.repositoryJPA = repositoryJPA;
    }

    @Override
    @Transactional
    public void guardar(Localidad localidad) {
        logger.debug("Guardo localidad {}", localidad);
        repositoryJPA.save(LocalidadEntity.fromDomain(localidad));
    }
}

